//
//  CDEmployee+CoreDataProperties.swift
//  CoreDataExample
//
//  Created by mac-00017 on 12/08/21.
//
//

import Foundation
import CoreData


extension CDEmployee {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDEmployee> {
        return NSFetchRequest<CDEmployee>(entityName: "CDEmployee")
    }

    @NSManaged public var firstname: String?
    @NSManaged public var gender: String?
    @NSManaged public var id: String?
    @NSManaged public var lastname: String?
    @NSManaged public var profilepic: Data?
    @NSManaged public var salary: String?
    @NSManaged public var toPassport: CDPassport?

    

}

extension CDEmployee : Identifiable {

}
