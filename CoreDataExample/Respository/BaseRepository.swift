//
//  ParentRepository.swift
//  CoreDataExample
//
//  Created by mac-00017 on 11/08/21.
//

import Foundation

protocol BaseRepository {
    associatedtype T
    func create(record: T)
    func getAll() -> [T]?
    func sortedData(with sortDiscriptor: [NSSortDescriptor]?) -> [T]?
    func get(byIdentifier id: String) -> T?
    func update(record: T) -> Bool
    func delete(id: String) -> Bool
}
