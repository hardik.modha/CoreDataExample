//
//  EmployeeDataRepository.swift
//  CoreDataExample
//
//  Created by mac-00017 on 10/08/21.
//

import Foundation

protocol EmployeeDataRepository: BaseRepository {}

struct EmployeeRepository: BaseRepository {



    func create(record: Employee) {
        let cdEmpoyee = CDEmployee(context: PersistentStorage.shared.context)
        cdEmpoyee.id = record.id
        cdEmpoyee.firstname = record.firstName
        cdEmpoyee.lastname = record.lastName
        cdEmpoyee.gender = record.gender.description
        cdEmpoyee.salary = record.salary
        cdEmpoyee.profilepic = record.profilePic

        PersistentStorage.shared.saveContext()
    }

    func getAll() -> [Employee]? {
        guard let result = PersistentStorage.shared.fetchManagedObject(managedObject: CDEmployee.self) else { return nil }

        return self.getEmployee(from: result)

    }

    func get(byIdentifier id: String) -> Employee? {
        guard let cdEmployee = self.fetchRecord(byIndentifier: id) else { return nil }
        return cdEmployee.convertToEmployee()
    }

    func update(record: Employee) -> Bool {
        guard let cdEmployee = self.fetchRecord(byIndentifier: record.id) else { return false }
        cdEmployee.firstname = record.firstName
        cdEmployee.lastname = record.lastName
        cdEmployee.salary = record.salary
        cdEmployee.gender = record.gender.description
        cdEmployee.profilepic = record.profilePic
        PersistentStorage.shared.saveContext()
        return true
    }

    func delete(id: String) -> Bool {
        guard let cdEmployee = self.fetchRecord(byIndentifier: id) else { return false }
        PersistentStorage.shared.context.delete(cdEmployee)
        PersistentStorage.shared.saveContext()
        return true
    }

    func sortedData(with sortDiscriptor: [NSSortDescriptor]?) -> [Employee]? {
        let fethcRequest = CDEmployee.fetchRequest()
        let sorted = NSSortDescriptor(key: "salary", ascending: true)
        fethcRequest.sortDescriptors = [sorted]
        do {
            let result = try PersistentStorage.shared.context.fetch(fethcRequest)
            return self.getEmployee(from: result)
        } catch _ {
            return nil
        }
    }

    private func getEmployee(from cdEmployee: [CDEmployee]) -> [Employee] {
        var employee: [Employee] = [Employee]()
        cdEmployee.forEach { cdEmployee in
            employee.append(cdEmployee.convertToEmployee())
        }
        return employee
    }

    private func fetchRecord(byIndentifier: String) -> CDEmployee? {

        let fetchRequest = CDEmployee.fetchRequest()
        let predicate = NSPredicate(format: "id == %@", byIndentifier)
        fetchRequest.predicate = predicate

        do {
            guard let cdEmployee = try PersistentStorage.shared.context.fetch(fetchRequest).first else {return nil }
            return cdEmployee

        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }


}
