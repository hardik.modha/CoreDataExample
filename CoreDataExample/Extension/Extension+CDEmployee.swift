//
//  Extension+CDEmployee.swift
//  CoreDataExample
//
//  Created by mac-00017 on 12/08/21.
//

import Foundation

extension CDEmployee {
    func convertToEmployee() -> Employee {
        return Employee(id: self.id!, firstName: self.firstname!, lastName: self.lastname!, gender: Gender(rawValue: self.gender!) ?? .male, salary: self.salary!, profilePic: self.profilepic ?? Data())
    }
    
}
