//
//  TextTblCell.swift
//  MVVMDemo
//
//  Created by mac-00021 on 12/10/20.
//  Copyright © 2020 mac-00021. All rights reserved.
//

import UIKit

class TextTblCell: UITableViewCell {
    
    @IBOutlet weak var txtField: PaddedFied!
    
    var onTextChange: ((String?, IndexPath) -> Void)?
    
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter  = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter
    }()
   
    private var indexPath: IndexPath?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        txtField.onTextChange = { [weak self] (text) in
            guard let strongSelf = self, let ip = self?.indexPath else { return }
            strongSelf.onTextChange?(text, ip)
            
        }
    }
    
    func configure(with fieldValue: FieldValue, indexPath: IndexPath) {
        self.indexPath = indexPath
        self.txtField.placeholder = fieldValue.placeholder
        self.txtField.text = fieldValue.value
        self.txtField.keyboardType = fieldValue.keyboardType
        
        if let datePicker = fieldValue.inputView as? UIDatePicker  {
            datePicker.addTarget(self, action: #selector(self.selecteDate(_datePicker:)), for: .valueChanged)
            if let value = fieldValue.value, let date = self.dateFormatter.date(from: value) {
                datePicker.date = date
            }
            self.txtField.inputView = datePicker
        }

        if let pickerView = fieldValue.inputView as? UIPickerView {
            self.txtField.text = fieldValue.value
            pickerView.delegate = self
            pickerView.dataSource = self
            self.txtField.inputView = pickerView
            guard let ip = self.indexPath else { return }
            self.onTextChange?(txtField.text, ip)
        }
        
    }
    
    @objc func selecteDate(_datePicker: UIDatePicker) {
        let  dateString = self.dateFormatter.string(from: _datePicker.date)
        self.txtField.text = dateString
        if let ip = self.indexPath {
            self.onTextChange?(dateString, ip)
        }
        
    }
    
}

extension TextTblCell: UIPickerViewDelegate {

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Gender.allCases[row].description
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.txtField.text = Gender.allCases[row].description
        guard let ip = self.indexPath else { return }
        self.onTextChange?(txtField.text, ip)
    }

}

extension TextTblCell: UIPickerViewDataSource {


    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Gender.allCases.count
    }


}
