//
//  StudentTblCell.swift
//  MVVMDemo
//
//  Created by mac-00021 on 13/10/20.
//  Copyright © 2020 mac-00021. All rights reserved.
//

import UIKit

class StudentTblCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.height / 2
        self.profileImageView.clipsToBounds = true
    }
    
    func configure(with employee: Employee) {
        self.lblName.text = employee.fullName
        self.lblGender.text = employee.gender.description
        self.profileImageView.image = employee.image
    }

}
