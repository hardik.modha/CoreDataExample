//
//  ImageCell.swift
//  CoreDataExample
//
//  Created by mac-00017 on 11/08/21.
//

import UIKit

class ImageCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var btnImage: UIButton!

    var onTapped: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.profileImageView.layer.borderColor = UIColor.black.cgColor
        self.profileImageView.layer.borderWidth = 0.5
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.height / 2
        self.profileImageView.clipsToBounds = true

        self.btnImage.backgroundColor = .lightGray.withAlphaComponent(0.5)
        self.btnImage.layer.borderColor = UIColor.black.cgColor
        self.btnImage.layer.borderWidth = 0.5
        self.btnImage.layer.cornerRadius = self.btnImage.frame.height / 2
        self.btnImage.clipsToBounds = true
    }


    @IBAction func pickedAction(_ sender: UIButton) {
        self.onTapped?()
    }

    
}
