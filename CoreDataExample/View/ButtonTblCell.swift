//
//  ButtonTblCell.swift
//  MVVMDemo
//
//  Created by mac-00021 on 13/10/20.
//  Copyright © 2020 mac-00021. All rights reserved.
//

import UIKit

class ButtonTblCell: UITableViewCell {
    
    @IBOutlet weak var button: UIButton!
    
    var onClick: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    func configure(value: ButtonValue) {
        button.setImage(value.image, for: UIControl.State.normal)
        button.setTitle(value.title, for: UIControl.State.normal)
        button.backgroundColor = value.backgroundColor
        button.tintColor = value.tintColor
    }

    
    @IBAction func btnAction(_ sender: UIButton) {
        self.onClick?()
    }
    
    
}
