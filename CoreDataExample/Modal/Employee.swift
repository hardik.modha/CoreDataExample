//
//  Employee.swift
//  CoreDataExample
//
//  Created by mac-00017 on 10/08/21.
//

import Foundation
import UIKit

enum Gender: String, CustomStringConvertible, CaseIterable {
    case male = "Male"
    case female = "Female"

    var description: String {
        return self.rawValue
    }
}

struct Employee {
    var id: String
    var firstName: String
    var lastName: String
    var gender: Gender
    var salary: String
    var profilePic: Data
    var passport: Passport? = nil
}

extension Employee {
    var fullName: String {
        return self.firstName + " " + self.lastName
    }

    var image: UIImage {
        return UIImage(data: profilePic) ?? UIImage()
    }
}
