//
//  SignUpRowType.swift
//  MVVMDemo
//
//  Created by mac-00021 on 12/10/20.
//  Copyright © 2020 mac-00021. All rights reserved.
//

import UIKit

enum SignUpRow: Int, CaseIterable {
    case image = 0
    case firstName 
    case lastName
    case gender
    case salary
    case button
}


extension SignUpRow {
    
    var fieldValue: FieldValue? {
        switch self {
        case .firstName:
            return FieldValue(placeholder: "FirstName", image: UIImage())
        case .lastName:
            return FieldValue(placeholder: "LastName", image: UIImage())
        case .gender:
            let pikcer = UIPickerView()
            return FieldValue(placeholder: "Gender", image: UIImage(), inputView: pikcer)
        case .salary:
            return FieldValue(placeholder: "Salary", image: UIImage())
        default:
            return nil
            
        }
    }
    
    var buttonValue: ButtonValue? {
        switch self {
        case .button:
            return ButtonValue(title: "Submit".uppercased(), image: nil, font: UIFont.systemFont(ofSize: 15.0, weight: .semibold), attributedTitle: nil, backgroundColor: UIColor.red, tintColor: UIColor.white)
        default:
            return nil
        }
    }
    
    var indexPath: IndexPath {
        return IndexPath(row: self.rawValue, section: 0)
    }
    
    var validationMessage: String? {
        switch self {
        case .firstName:
            return "Please enter firstName."
        case .lastName:
            return "Please enter lastName"
        case .gender:
            return "Please select gender"
        case .salary:
            return "Please enter salary"
        default:
            return nil
        }
    }
    
}
