//
//  Passport.swift
//  CoreDataExample
//
//  Created by mac-00017 on 12/08/21.
//

import Foundation


struct Passport {
    var id: UUID
    var passportId: String
    var placeOfIssue: String
    var name: String? = nil
}
