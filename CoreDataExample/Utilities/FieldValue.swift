//
//  FieldValue.swift
//  MVVMDemo
//
//  Created by mac-00021 on 12/10/20.
//  Copyright © 2020 mac-00021. All rights reserved.
//

import UIKit

struct FieldValue {
    var placeholder: String
    var image: UIImage
    var value: String?
    var keyboardType: UIKeyboardType = .default
    var isSecured: Bool = false
    var inputView: UIView?
}

struct ButtonValue {
    var title: String
    var image: UIImage?
    var font: UIFont
    var attributedTitle: NSAttributedString?
    var backgroundColor: UIColor?
    var tintColor: UIColor
}



