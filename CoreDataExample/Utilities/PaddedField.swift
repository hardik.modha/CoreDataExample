//
//  PaddedField.swift
//  MVVMDemo
//
//  Created by mac-00021 on 12/10/20.
//  Copyright © 2020 mac-00021. All rights reserved.
//

import UIKit


class PaddedFied: UITextField {
    
    var onTextChange: ((String?) -> Void)?
    
    @IBInspectable var padding: CGFloat = 8.0 {
        didSet {
            self.updatePadding()
        }
    }
    
    lazy var paddedView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.padding, height: self.frame.height))
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    private func setup() {
        NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: nil, queue: nil) { [weak self] (notification) in
            guard let strongSelf = self else { return }
            guard let textField =  notification.object as? UITextField, textField === self else { return }
            strongSelf.onTextChange?(textField.text)
        }
        self.paddedView.backgroundColor = .clear
        self.leftView = self.paddedView
        self.leftViewMode = .always
        self.updatePadding()
    }
    
    private func updatePadding() {
        var width = self.paddedView.frame.width
        width += self.padding
        self.paddedView.frame.size.width = width
    }
    
    
    
}
