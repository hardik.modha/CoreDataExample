//
//  RegisterEmployeeManager.swift
//  CDcrudOperations
//
//  Created by CodeCat15 on 6/19/20.
//  Copyright © 2020 CodeCat15. All rights reserved.
//

import Foundation
import CoreData

struct EmployeeManager {

    private let repository = EmployeeRepository()

    func createEmployee(employee: Employee) {
        repository.create(record: employee)
    }

    func fetchEmployee() -> [Employee]? {
        repository.getAll()
    }

    func getEmployee(byIdentifier: String) -> Employee? {
        return repository.get(byIdentifier: byIdentifier)
    }

    func updateEmployee(employee: Employee) -> Bool {
        let result = repository.update(record: employee)
        return result
    }

    func deleteEmployee(employee: Employee) -> Bool {
        let result = repository.delete(id: employee.id)
        return result
    }

    
}
