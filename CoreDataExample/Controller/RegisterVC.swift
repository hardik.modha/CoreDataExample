//
//  RegisterVC.swift
//  CoreDataExample
//
//  Created by mac-00017 on 10/08/21.
//

import UIKit
import RxSwift

enum ScreenMode {
    case add
    case update(Employee)

    var employee: Employee? {
        switch self {
        case .add:
            return nil
        case .update(let emp):
            return emp
        }
    }

    var isUpdate: Bool {
        switch self {
        case .add:
            return false
        case .update:
            return true
        }
    }

    var title: String {
        switch self {
        case .add:
            return "Add Employee"
        case .update:
            return "Update Employee"
        }
    }
}

class RegisterVC: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak private var tableView: UITableView!

    private let employeeManager = EmployeeManager()

    private var datasouce: [SignUpRow] = SignUpRow.allCases
    private var firstName: String = String()
    private var lastName: String = String()
    private var gender: String = String()
    private var salary: String = String()

    private var fieldCashe: [IndexPath: String] = [:]
    private var imageCashe: [IndexPath: UIImage] = [:]
    private var image: UIImage = UIImage()
    var onSuccess: (() -> Void)?

    var mode: ScreenMode = .add

    //MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.mode.title
        self.setupTableView()
        self.setupEmployee()
    }

    //MARK: - Private Methods
    private func setupTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 80
        self.tableView.separatorStyle = .none
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.registerNib(TextTblCell.self)
        self.tableView.registerNib(ImageCell.self)
        self.tableView.registerNib(ButtonTblCell.self)
    }

    private func setupEmployee() {
        guard let employee = self.mode.employee else { return }
        for item in self.datasouce {
            switch item {
            case .image:
                self.image = employee.image
            case .firstName:
                self.fieldCashe[item.indexPath]  = employee.firstName
            case .lastName:
                self.fieldCashe[item.indexPath]  = employee.lastName
            case .salary:
                self.fieldCashe[item.indexPath]  = employee.salary
            case .gender:
                self.fieldCashe[item.indexPath]  = employee.gender.description
            case .button:
                break
            }
        }
    }

    private func validation() -> Bool {
        for item in self.datasouce {
            switch item {
            case .firstName:
                if let value = self.fieldCashe[item.indexPath], !value.isEmpty {
                    self.firstName = value
                } else {
                    print(item.validationMessage!)
                    return false
                }
            case .lastName:
                if let value = self.fieldCashe[item.indexPath], !value.isEmpty {
                    self.lastName = value
                } else {
                    print(item.validationMessage!)
                    return false
                }
            case .gender:
                if let value = self.fieldCashe[item.indexPath], !value.isEmpty {
                    self.gender = value
                } else {
                    print(item.validationMessage!)
                    return false
                }
            case .salary:
                if let value = self.fieldCashe[item.indexPath], !value.isEmpty {
                    self.salary = value
                } else {
                    print(item.validationMessage!)
                    return false
                }
            default:
                break
            }
        }
        return true

    }

    private func openImagePicker() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }

    private func add() {
        if validation(){
            let imgData = self.image.jpegData(compressionQuality: 0.4)

            let employee = Employee(id: UUID().uuidString, firstName: self.firstName, lastName: self.lastName, gender: Gender(rawValue: self.gender) ?? .male, salary: self.salary, profilePic: imgData ?? Data())
            employeeManager.createEmployee(employee: employee)
            self.dismiss(animated: true, completion: nil)
            self.onSuccess?()
        }

    }

    private func updateEmployee() {
        if validation() {
            guard var employee = self.mode.employee else { return }
            let imgData = self.image.jpegData(compressionQuality: 0.4)
            employee.firstName = self.firstName
            employee.lastName = self.lastName
            employee.gender = Gender(rawValue: self.gender) ?? .male
            employee.salary = self.salary
            employee.profilePic = imgData ?? Data()
            let result = employeeManager.updateEmployee(employee: employee)
            if result {
                self.dismiss(animated: true, completion: nil)
                self.onSuccess?()
            }
        }
    }

    deinit {
        print(#function, "called for", #file)
    }

}

//MARK: - UITableViewDelegate
extension RegisterVC: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = self.datasouce[indexPath.row]

        switch item {
        case .image:
            return 210.0
        default:
            return UITableView.automaticDimension
        }
    }

}

//MARK: - UITableViewDataSource
extension RegisterVC: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.datasouce.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.datasouce[indexPath.row]

        switch item {
        case .image:
            if let cell = tableView.dequeueReuseableCell(ImageCell.self, at: indexPath) {
                cell.onTapped = {
                    self.openImagePicker()
                }
                cell.profileImageView.image = self.image
                return cell
            }
        case .firstName, .lastName, .salary, .gender:
            if let cell = tableView.dequeueReuseableCell(TextTblCell.self, at: indexPath) {
                cell.onTextChange = { [weak self] (text, indexPath) in
                    self?.fieldCashe[indexPath] = text
                }

                if var fieldValue = item.fieldValue {
                    fieldValue.value = self.fieldCashe[indexPath]
                    cell.configure(with: fieldValue, indexPath: indexPath)
                }

                return cell
            }
            break
        case .button:
            if let cell = tableView.dequeueReuseableCell(ButtonTblCell.self, at: indexPath) {
                cell.onClick = { [weak self] in
                    guard let strongSelf = self else { return }
                    if strongSelf.mode.isUpdate {
                        strongSelf.updateEmployee()
                    } else {
                        strongSelf.add()
                    }
                }
                if let value = item.buttonValue {
                    cell.configure(value: value)
                }
                return cell
            }

        }

        return UITableViewCell()
    }


}

//MARK: - UIImagePickerControllerDelegate
extension RegisterVC: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        if let image = info[.originalImage] as? UIImage {
            self.image = image
            self.tableView.reloadData()
            picker.dismiss(animated: true, completion: nil)
        }

    }
}
