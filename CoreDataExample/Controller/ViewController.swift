//
//  ViewController.swift
//  CoreDataExample
//
//  Created by mac-00017 on 10/08/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    private var datasource: [Employee] = [Employee]() {
        didSet {
            self.tableView.reloadData()
        }
    }

    private let empoyeeManager = EmployeeManager()

    lazy var addButton: UIBarButtonItem = {
        let button = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addAction(_:)))
        return button
    }()



    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = self.addButton
        self.setupTableView()
        self.fetchEmployee()
    }

    private func fetchEmployee() {
        self.datasource.removeAll()
        guard let employees = empoyeeManager.fetchEmployee() else { return }
        self.datasource = employees
    }

    private func setupTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.registerNib(StudentTblCell.self)
        self.tableView.estimatedRowHeight = 80
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.separatorStyle = .singleLine
        self.tableView.tableFooterView = UIView()

    }

    private func navigateToRegister(with mode: ScreenMode) {
        let registerVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        registerVC.title = "Register"
        registerVC.mode = mode
        registerVC.onSuccess = {
            self.fetchEmployee()

        }
        let navigationController: UINavigationController = UINavigationController(rootViewController: registerVC)
        self.present(navigationController, animated: true, completion: nil)
    }


    @objc func addAction(_ sender: UIBarButtonItem) {
        self.navigateToRegister(with: .add)
    }

    func deleteEmployee(employee: Employee) {
        let result = self.empoyeeManager.deleteEmployee(employee: employee)
        if result {
            self.fetchEmployee()
        }
    }




}

extension ViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        UISwipeActionsConfiguration(actions: [UIContextualAction(style: .destructive, title: "Delete", handler: { action, view, completion in
            let employee = self.datasource[indexPath.row]
            self.deleteEmployee(employee: employee)
            completion(true)
        })])
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigateToRegister(with: .update(self.datasource[indexPath.row]))
    }

}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.datasource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReuseableCell(StudentTblCell.self, at: indexPath) {
            cell.configure(with: self.datasource[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
}
